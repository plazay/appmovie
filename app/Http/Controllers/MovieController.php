<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Movies;
use Laravel\Passport\ClientRepository;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;

class MovieController extends Controller
{
    private static function object_to_array($obj) 
    {
        if(is_object($obj)) $obj = (array) $obj;
        if(is_array($obj)) {
            $new = array();
            foreach($obj as $key => $val) {
                $new[$key] = self::object_to_array($val);
            }
        }
        else $new = $obj;
        return $new;
    }
    public function getDataMovie(Request $request)
        {
            // $datosTracking = Movies::getData();
        
            // return response()->json($datosTracking); 
            $client = new \GuzzleHttp\Client();
            $request = $client->get('http://www.omdbapi.com/?i=tt3896198&apikey=eeb17b81');
            $dataMovies = $request->getBody();
            
            $temp = static::object_to_array($dataMovies);

            
            return view('movieList', compact('temp'));
        }
}