<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    
    <title>AppMovie </title>
    
    <link href="css/bootstrap.css" rel="stylesheet" />
	<link href="css/coming-sssoon.css" rel="stylesheet" />    
    
    <!--     Fonts     -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
  
</head>

<body>
<nav class="navbar navbar-transparent navbar-fixed-top" role="navigation">  
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">

      </ul>
      <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="#"> 
                    <i class="fa fa-facebook-square"></i>
                    facebook
                </a>
            </li>
             <li>
                <a href="#"> 
                    <i class="fa fa-twitter"></i>
                    Tweet
                </a>
            </li>
             <li>
                <a href="#"> 
                    <i class="fa fa-envelope-o"></i>
                    Email
                </a>
            </li>
       </ul>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container -->
</nav>
<div class="main" style="background-image: url('images/peli.jpg')">
    
    <div class="cover blue" data-color="blue"></div>

    <div class="container">
        <h1 class="logo cursive">
            Lista de peliculas
        </h1>
        
        <div class="content">
            <!-- <h4 class="motto">Donde podras buscar tus peliculas favoritas.</h4> -->
            <div class="col-md-12 mr-3 no-margin">
                <div class="modal-header">
                <h4 class="title">Detalles de pedidos</h4>
                <p class="category"></p>
                </div><br>
                <div class="table-responsive">
                <table class="table table-striped" width="100%" >
                    <thead>
                    <tr>
                        <th class="th-sm">Titulo</th>
                        <th class="th-sm">Año</th>
                        <th class="th-sm">Imagen</th>
                        <th class="th-sm">Genero</th>
                        <th class="th-sm">Fecha de lanzamiento</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$temp->Title}} </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
      <div class="container">
             Creado por un <a href="#">Amante de las buenas peliculas <i class="fa fa-heart heart"></i>
      </div>
    </div>
 </div>
 </body>
   <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
   <script src="js/bootstrap.min.js" type="text/javascript"></script>

</html>